local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local util = import 'gitlab.com/cocainefarm/k8s/lib/util/main.libsonnet';

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);

(util.inlineSpec('https://10.10.0.1:6443', 'namespace', null, null)) + {
  data: {
    'chart': helm.template('chart', 'charts/chart', {
      namespace: 'postgres',
      values: {
      }
    }),
  },
}
